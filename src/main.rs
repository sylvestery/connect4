use std::io;
use std::io::Write;
use std::result::Result::Err;


fn print_board(board: &Vec<Vec<char>>) {
    for row in (0..board.len()).rev() {
        for col in 0..board[row].len() {
            print!("{}", board[row][col]);
        }
        println!();
    }
}

fn take_num_input(input: &String) -> usize {
    match input.trim().parse() {
        Ok(i) => i,
        Err(..) => panic!("NOT AN INTEGER."),
    }
}

fn take_input_and_flush(output: String) -> String {
    let mut input_buffer = String::new();
    let stdin = io::stdin();

    print!("{}", output);
    io::stdout().flush().expect("Flush Failure!");
    stdin.read_line(&mut input_buffer).expect("Failed to read input.");
    input_buffer
}

fn insert_chip(board: &mut Vec<Vec<char>>, col: usize, chip_type: char) -> usize {
    let row = 0;
    for row in 0..board.len() {
        if board[row][col] == '-' {
            board[row][col] = chip_type;
            break;
        }
    }
    row
}

fn check_draw(board: &Vec<Vec<char>>) -> bool {
    let mut is_draw: bool = true;

    for row in board.iter() {
        for col in row.iter() {
            if col.to_owned() == '-' {
                is_draw = false;
                break;
            } else {
                continue;
            }
        }
    }
    return is_draw;
}

fn check_if_winner(board: &Vec<Vec<char>>, col: usize, row: usize, chip_type: char) -> bool {
    let mut count = 0;

    for i in 0..board[row].len() {
        if board[row][i] == chip_type {
            count += 1;
        } else {
            break;
        }
    }

    if count >= 4 {
        return true;
    }

    count = 0;

    for i in 0..board.len() {
        if board[i][col] == chip_type {
            count += 1;
        } else {
            break;
        }
    }
//TODO Implement Diagonals.
    return count >= 4;
}

fn main() {
    let mut input: String;
    //  print!("What would you like the height of the board to be? ");
    // io::stdout().flush().expect("flush failed!");

    input = take_input_and_flush("What would you like the height of the board to be? ".to_string());
    //input.read_line(&mut input_buffer).expect("Failed to read first input.");
    let height = take_num_input(&input);
    input = take_input_and_flush("What would you like the width of the board to be? ".to_string());
    let width = take_num_input(&input);


    let mut board = Vec::new();
    let mut b2 = Vec::new(); //The new vec that will be forces into the last vec.

    for _i in 0..height {
        b2 = Vec::new();
        for _j in 0..width {
            b2.push('-');
        }
        board.push(b2);
    }

    print_board(&board);

    let mut col: usize;
    let mut row: usize;
    let mut chip_type: char = 'o';
    loop {
        if chip_type == 'o' {
            col = take_num_input(
                &take_input_and_flush("Player 1: Which column would you like to choose? "
                    .to_string())
            );
            chip_type = 'x';
        } else {
            col = take_num_input(
                &take_input_and_flush("Player 2: Which column would you like to choose? "
                    .to_string())
            );
            chip_type = 'o';
        }

        row = insert_chip(&mut board, col, chip_type);

        print_board(&board);

        if check_if_winner(&board, col, row, chip_type) {
            if chip_type == 'x' {
                println!("Player 1 won the game!");
            } else {
                println!("Player 2 won the game!");
            }
            break;
        } else if check_draw(&board) {
            println!("Draw. Nobody wins.");
            break;
        }
    }
}